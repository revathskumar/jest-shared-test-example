import React, { Component } from 'react';

class FormA extends Component {
  constructor() {
      super();
      this.state = {
        errors: {},
        fields: {}
      }

      this.onChange = this.onChange.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
  }
  
  onSubmit(e) {
    e.preventDefault();
    this.setState({ errors: {} });
    let errors = {};
    if (!this.state.fields.name) {
      errors = {name: "Name is Required" };
    }
    if (!this.state.fields.age) {
      errors = {...errors, age: "Age is Required" };
    }
    this.setState({ errors });
  }
  
  onChange(e) {
    const fields = {...this.state.fields, [e.target.name]: e.target.value};
    this.setState({ fields });
  }
  
  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <label>Name </label>
          <input type="text" name="name" value={this.state.fields.name} onChange={this.onChange} />
          <div className="error-message">{this.state.errors.name}</div>
          <label>Age </label>
          <input type="text" name="age" value={this.state.fields.age} onChange={this.onChange} />
          <div className="error-message">{this.state.errors.age}</div>
          <button type="submit">Submit</button>
        </form>
      </div>
    );
  }
}

export default FormA;