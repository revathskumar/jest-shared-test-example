import React from "react";
import { shallow } from 'enzyme';
import fakeEvent from 'fake-event';

export const commonFormValidation = function (Form) {
    test('render name error message', () =>{
        const component = shallow(<Form {...this.commonProps} />);
        component.setState({ fields: {age: 12, gender: 'male'} });
        component.find('form').simulate('submit', fakeEvent());
        expect(component.text()).toEqual(expect.stringContaining('Name is Required'));
    });

    test('render age error message', () => {
        const component = shallow(<Form {...this.commonProps} />);
        component.setState({ fields: {name: 'Name', gender: 'male'} });
        component.find('form').simulate('submit', fakeEvent());
        expect(component.text()).toEqual(expect.stringContaining('Age is Required'));
    });
}

export const commonFormOnUpdate = function (Form) {
    test('update name state', () =>{
        const component = shallow(<Form {...this.commonProps} />);
        component.find('input[name="name"]').simulate('change', fakeEvent({ target: { name: 'name', value: 'Name'} }));
        expect(component.state('fields').name).toEqual('Name');
    });

    test('update age state', () =>{
        const component = shallow(<Form {...this.commonProps} />);
        component.find('input[name="age"]').simulate('change', fakeEvent({ target: { name: 'age', value: 20} }));
        expect(component.state('fields').age).toEqual(20);
    });
}