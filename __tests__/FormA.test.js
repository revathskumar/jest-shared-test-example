import FormA from '../FormA';
import { commonFormValidation, commonFormOnUpdate } from '../test/shared/shouldBehaveLikeForm';

describe('<FormA />', () => {
    beforeEach(() =>{
        this.commonProps = {};
    });

    describe('render error messages', () => {
        commonFormValidation.bind(this)(FormA);
    });

    describe('change events update states', () =>{
        commonFormOnUpdate.bind(this)(FormA);
    });
});