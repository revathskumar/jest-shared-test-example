import React from 'react';
import { shallow } from 'enzyme';
import fakeEvent from 'fake-event';

import FormB from '../FormB';
import { commonFormValidation, commonFormOnUpdate } from '../test/shared/shouldBehaveLikeForm';

describe('<FormB />', () => {
    beforeEach(() =>{
        this.commonProps = {};
    });

    describe('render error messages', () => {
        commonFormValidation.bind(this)(FormB);

        test('render age error message', () => {
            const component = shallow(<FormB {...this.commonProps} />);
            component.setState({ fields: {name: 'Name', age: 12} });
            component.find('form').simulate('submit', fakeEvent());
            expect(component.text()).toEqual(expect.stringContaining('Gender is Required'));
        });
    });

    describe('change events update states', () =>{
        commonFormOnUpdate.bind(this)(FormB);

        test('update Gender state', () =>{
            const component = shallow(<FormB {...this.commonProps} />);
            component.find('input[name="gender"]').simulate('change', fakeEvent({ target: { name: 'gender', value: 'male'} }));
            expect(component.state('fields').gender).toEqual('male');
        });
    });
});